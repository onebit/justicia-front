import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Productos from '@/components/Productos'
import ProductosCrear from '@/components/Productos-crear'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/Productos',
      name: 'Productos',
      component: Productos
    },
    {
      path: '/Productos/crear',
      name: 'Crear producto',
      component: ProductosCrear
    }
  ]
})
