// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/es'
import 'element-ui/lib/theme-chalk/index.css'
import selectProducto from './components/_control/select-producto.vue'
Vue.config.productionTip = false
Vue.use(ElementUI, { locale })
Vue.use(VueResource)
Vue.component('select-producto', selectProducto)
Vue.http.headers.common['Authorization'] = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBlcGUiLCJleHAiOjE1MzYzMzI2NzgsImlzcyI6IllPIiwiYXVkIjoiQVBQUyJ9.8jtZI3QYybHPcdYNHKoEy9ol_aMWb4Zx80FuCdbFW6g'
Vue.http.options.emulateHTTP = true
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
